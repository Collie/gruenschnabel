# Beschreibung

Grünschnabel ist eine Website zum Kennenlernen von verschiedenen Vogelarten. In einer Übersicht können die verschiedenen Arten und ihre Rufe abgerufen werden, im Quiz kann die Erkennung der Vögel anhand ihres Rufs oder ihres Aussehens geübt werden.

Die Seite ist quelloffen. Die Vogelrufe sind entweder offen oder mit expliziter Erlaubnis des Urhebers ausgewählt. Sollte eine dazugehörige Lizenz oder Urheberangabe existieren, ist diese in den Details des betreffenden Vogels zu finden.

## Tools

### Platformen

- Figma für die Modellierung der UI
- VSCode für die Projektdurchführung
- GitLab als Organisationstool und Repository

### Sprachen / Frameworks

#### Frontend

- HTML, CSS und JS für die grundlegende Seite
- Es ist geplant, diese Seite als React-Anwendung umzubauen, sobald Design und Funktionalität stehen.

#### Backend

- Spring Boot / Java Backend

### Sonstige

- (Future) Gitlab CI für die Ausführung von Runnern zum automatischen build und deploy der Seite sowie Versionierung
- Google Material Icons als freiere Alternative zu Fontawesome
```
