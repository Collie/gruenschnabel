package it.collmann.gruenschnabel.dto;

public class BirdDto {
	private Long id;

	private String name;

	private String infoSourceURI;

	private String infoSourceText;

	private String infoText;

	private BirdPictureDto birdPicture;

	private BirdSoundDto birdSound;
}
