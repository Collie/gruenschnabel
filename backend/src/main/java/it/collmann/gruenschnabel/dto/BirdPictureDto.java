package it.collmann.gruenschnabel.dto;

public class BirdPictureDto {
	private Long id;

	private String pictureURI;

	private String author;

	private String altText;
}
