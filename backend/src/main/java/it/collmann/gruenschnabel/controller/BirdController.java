package it.collmann.gruenschnabel.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import it.collmann.gruenschnabel.dto.BirdDto;
import it.collmann.gruenschnabel.model.Bird;
import it.collmann.gruenschnabel.repository.BirdRepository;

@RestController
public class BirdController {

	@Autowired
	BirdRepository birdRepository;

	@Autowired
	private ModelMapper modelMapper;

	@GetMapping("/birds")
	public ResponseEntity<List<BirdDto>> getAllBirds() {
		List<BirdDto> birds = birdRepository.findAll().stream().map(this::convertToDto).collect(Collectors.toList());
		return new ResponseEntity<>(birds, HttpStatus.OK);

	}

	// TODO move to service
	private BirdDto convertToDto(Bird bird) {
		return modelMapper.map(bird, BirdDto.class);
	}

//	@GetMapping("/birds/{id}")
//	public ResponseEntity<BirdDto> getBird(@PathVariable("id") Long id) {
//
//	}
}
