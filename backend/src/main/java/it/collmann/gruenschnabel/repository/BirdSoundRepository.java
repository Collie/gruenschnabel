package it.collmann.gruenschnabel.repository;

import org.springframework.data.repository.CrudRepository;

import it.collmann.gruenschnabel.model.BirdSound;

public interface BirdSoundRepository extends CrudRepository<BirdSound, Long> {

}
