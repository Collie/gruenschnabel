package it.collmann.gruenschnabel.repository;

import org.springframework.data.repository.CrudRepository;

import it.collmann.gruenschnabel.model.BirdPicture;

public interface BirdPictureRepository extends CrudRepository<BirdPicture, Long> {

}
