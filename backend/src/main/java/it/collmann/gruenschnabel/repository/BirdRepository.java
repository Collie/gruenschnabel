package it.collmann.gruenschnabel.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import it.collmann.gruenschnabel.model.Bird;

public interface BirdRepository extends CrudRepository<Bird, Long> {
	Optional<Bird> findById(Long id);

	List<Bird> findAll();

	List<Bird> findByName(String name);
}
