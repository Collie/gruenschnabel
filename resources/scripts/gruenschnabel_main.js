var infoButton = document.getElementById("bird-modal-info");
infoButton.onclick = function () {
  let info = document.getElementById("bird-modal-info-active");
  if (info.style.visibility == "" || info.style.visibility == "hidden") {
    // use "" instead of "none"
    info.style.visibility = "visible";
  } else {
    info.style.visibility = "hidden";
  }
};

var audio = new Audio("resources/sound/zilpzalp.mp3");

var exitModal = document.getElementById("bird-modal-exit");
exitModal.onclick = function () {
  closeModal();
};

var sound = new Audio('resources/sound/birds/zilpzalp.mp3');
sound.volume = 0.1;

document.querySelectorAll(".play").forEach((item) => {
    item.addEventListener("click", function(event) {
      if(!sound.paused) {
        sound.pause();
      } else {
              sound.play();
      }
      event.stopPropagation();
    });
});

document.querySelectorAll(".bird-card").forEach((item) => {
  item.addEventListener("click", function(event) {
    openModal();
    event.stopPropagation();
  });
});

document.addEventListener("click", (e) => {
  if (e.target == document.getElementById("bird-modal")) {
    closeModal();
  }
});

function closeModal() {
  document.getElementById("bird-modal").classList.add("no-transition");
  document.getElementById("bird-modal").classList.remove("is-visible");
  document.body.classList.remove("no-scroll");
  document.getElementById("bird-modal").classList.remove("no-transition");
  document.getElementById("bird-modal-info-active").style.visibility = "hidden";
  stopAudio();
}

function openModal() {
  document.getElementById("bird-modal").classList.add("is-visible");
  document.body.classList.add("no-scroll");
}

function playSound() {
  audio.play();
}

function stopAudio() {
  audio.pause();
  audio.currentTime = 0;
}